# API Server

Simple Rest API using gin(framework) & gorm(orm)

## Endpoint list

### Chats Resource

```
GET    /chats
GET    /chats/:id
POST   /chats
PUT    /chats/:id
DELETE /chats/:id
```

### UserChats Resource

```
GET    /userchats
GET    /userchats/:id
POST   /userchats
PUT    /userchats/:id
DELETE /userchats/:id
```

server runs at http://localhost:8080
