package controllers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func APIEndpoints(c *gin.Context) {
	reqScheme := "http"

	if c.Request.TLS != nil {
		reqScheme = "https"
	}

	reqHost := c.Request.Host
	baseURL := fmt.Sprintf("%s://%s", reqScheme, reqHost)

	resources := map[string]string{
		"chats_url":      baseURL + "/chats",
		"chat_url":       baseURL + "/chats/{id}",
		"user_chats_url": baseURL + "/user_chats",
		"user_chat_url":  baseURL + "/user_chats/{id}",
	}

	c.IndentedJSON(http.StatusOK, resources)
}
