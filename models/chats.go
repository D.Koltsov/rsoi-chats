package models

import "time"

type Chat struct {
	ID            uint       `gorm:"primary_key;AUTO_INCREMENT" json:"id" form:"id"`
	LastMessageID uint       `json:"last_message_id" form:"last_message_id"`
	CreatedAt     *time.Time `json:"created_at" form:"created_at"`
	UpdatedAt     *time.Time `json:"updated_at" form:"updated_at"`
}
