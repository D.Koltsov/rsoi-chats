package models

import "time"

type UserChat struct {
	ID        uint       `gorm:"primary_key;AUTO_INCREMENT" json:"id" form:"id"`
	ChatID    uint       `json:"chat_id" form:"chat_id"`
	UserID    uint       `json:"user_id" form:"user_id"`
	CreatedAt *time.Time `json:"created_at" form:"created_at"`
	UpdatedAt *time.Time `json:"updated_at" form:"updated_at"`
}
