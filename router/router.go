package router

import (
	"gitlab.com/D.Koltsov/rsoi-chats/controllers"

	"github.com/gin-gonic/gin"
)

func Initialize(r *gin.Engine) {
	r.GET("/", controllers.APIEndpoints)

	api := r.Group("")
	{

		api.GET("/chats", controllers.GetChats)
		api.GET("/chats/:id", controllers.GetChat)
		api.POST("/chats", controllers.CreateChat)
		api.POST("/chats/:id", controllers.UpdateChatLastMsg)
		api.PUT("/chats/:id", controllers.UpdateChat)
		api.DELETE("/chats/:id", controllers.DeleteChat)

		api.GET("/user_chats", controllers.GetUserChats)
		api.GET("/user_chats/:id", controllers.GetUserChat)
		api.POST("/user_chats", controllers.CreateUserChat)
		api.PUT("/user_chats/:id", controllers.UpdateUserChat)
		api.DELETE("/user_chats/:id", controllers.DeleteUserChat)

	}
}
