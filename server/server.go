package server

import (
	"gitlab.com/D.Koltsov/rsoi-chats/middleware"
	"gitlab.com/D.Koltsov/rsoi-chats/router"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

func Setup(db *gorm.DB) *gin.Engine {
	r := gin.Default()
	r.Use(middleware.SetDBtoContext(db))
	router.Initialize(r)
	return r
}
